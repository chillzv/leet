package reverse_number

import (
	"fmt"
	"math"
	"strconv"
)

func Reverse(x int) int {
	xString := fmt.Sprint(x)
	xByte := []byte(xString)

	var reversedBytes []byte

	minus := []byte("-")[0]
	if xByte[0] == minus {
		reversedBytes = append(reversedBytes, minus)
	}
	for i := 1; i <= len(xByte); i++ {
		if xByte[len(xByte)-i] == minus {
			continue
		}
		reversedBytes = append(reversedBytes, xByte[len(xByte)-i])
	}

	xInt, err := strconv.Atoi(string(reversedBytes))
	if err != nil {
		return 0
	}

	if xInt > math.MaxInt32 || xInt < math.MinInt32 {
		return 0
	}

	return xInt

}
